#!/bin/bash
#Step 1 Create keypair for kafka0 server and so on.....
keytool -genkeypair -keystore kafka3.Server.keystore.jks -alias kafka3 -validity 365 -storepass Kafka3Password -keypass Kafka3Password -dname "CN=kafka3, OU= Kafka ordering service, O=fujitsu_bank, L=Capellen, S=Capellen, C=Lu"
keytool -importkeystore -srckeystore kafka3.Server.keystore.jks -destkeystore kafka3.Server.keystore.jks -deststoretype pkcs12 -storepass Kafka3Password
#Step 2 Create our own CA 

#openssl req -new -x509 -keyout ca-key -out ca-cert -days 365 -outform PEM
# I'm using instead  the orderer CA certificate and PK
keytool -keystore kafka.server.truststore.jks -alias CARoot -import -file ca.fujitsu_bank.lu-cert.pem -storepass KafkaCaRootServer
keytool -importkeystore -srckeystore kafka.server.truststore.jks -destkeystore kafka.server.truststore.jks -deststoretype pkcs12 


#Step 3  
#=========================export the certificate from the keystore======================
keytool -keystore kafka3.Server.keystore.jks -alias kafka3 -certreq -file kafka3-cert.pem -storepass Kafka3Password

#=========================Signing kafka0 certificate====================================
openssl x509 -req -CA ca.fujitsu_bank.lu-cert.pem -CAkey 7c6c24c1157376de216dd8145fa48aa2f3af522257e4770f7dd3b2799df09e2d_sk -in kafka3-cert.pem -out kafka3-cert-signed.pem -days 365 -CAcreateserial -passin pass:KafkaCaRootServer
#===============Import the Ca certificate in the keystore of kafka3======================================
keytool -keystore kafka3.Server.keystore.jks -alias CARoot -import -file ca.fujitsu_bank.lu-cert.pem -storepass Kafka3Password
#===============Import the kafka signed certificate in the keystore of kafka3============================
keytool -keystore kafka3.Server.keystore.jks -alias kafka3 -import -file kafka3-cert-signed.pem -storepass Kafka3Password

#===============At the least import Ca certificate in the Ca keystore======================
keytool -keystore kafka.server.truststore.jks -alias CARoot -import -file ca.fujitsu_bank.lu-cert.pem -storepass KafkaCaRootServer

#=================Do this for each orderer on the network=============================================
keytool -genkeypair -keystore orderer.Server.keystore.jks -alias orderer -validity 365 -storepass OrdererPassword -keypass OrdererPassword -dname "CN=orderer, OU= Kafka ordering service, O=fujitsu_bank, L=Capellen, S=Capellen, C=Lu"

keytool -importkeystore -srckeystore orderer.Server.keystore.jks -destkeystore orderer.Server.keystore.jks -deststoretype pkcs12 -storepass OrdererPassword

keytool -keystore orderer.Server.keystore.jks -alias orderer -certreq -file orderer-cert.pem -storepass OrdererPassword

openssl x509 -req -CA ca.fujitsu_bank.lu-cert.pem -CAkey 7c6c24c1157376de216dd8145fa48aa2f3af522257e4770f7dd3b2799df09e2d_sk -in orderer-cert.pem -out orderer-cert-signed.pem -days 365 -CAcreateserial -passin pass:KafkaCaRootServer

keytool -keystore orderer.Server.keystore.jks -alias CARoot -import -file ca.fujitsu_bank.lu-cert.pem -storepass OrdererPassword

keytool -keystore orderer.Server.keystore.jks -alias orderer -import -file orderer-cert-signed.pem -storepass OrdererPassword

#===========================example for kafka==========================
keystore password : Kafka0Password

password for alias kafka0 : Kakfka0Password

Enter PEM pass phrase:This is Jean Mathieu Tchamdjeu

#============================example kafka Ca-Root server================
keystore password : KafkaCaRootServer


#===========================Test the secured Kafka server=================
openssl s_client -debug -connect localhost:9093 -tls1_2

#========================generate key and cert==================================
openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout ca.key -out ca.pem -dname "CN=orderer, OU= Kafka ordering service, O=fujitsu_bank, L=Capellen, S=Capellen, C=Lu"


