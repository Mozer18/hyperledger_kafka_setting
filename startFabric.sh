#!/bin/bash

# Exit on first error, print all commands.
set -ev

#Detect architecture
ARCH=`uname -m`

# Grab the current directory
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

#

ARCH=`uname -m` docker-compose -f "${DIR}"/docker-compose-cli.yaml down
ARCH=`uname -m` docker-compose -f "${DIR}"/docker-compose-cli.yaml up -d

# wait for Hyperledger Fabric to start
# incase of errors when running later commands, issue export FABRIC_START_TIMEOUT=<larger number>
echo 15
sleep 15

#------------------------create the channel with peer0 of fujitsu_bank.lu---------------------------------------------
#docker exec peer0.fujitsu_bank.lu peer channel create -c bankschannel --orderer orderer.fujitsu_bank.lu:7050 -f /etc/hyperledger/configtx/bankchannel.tx 


#------------------------------Join peer0.fujitsu_bank.lu to the channel-----------------------------------
#docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@fujitsu_bank.lu/msp" peer0.fujitsu_bank.lu peer channel join -b bankschannel.block


#------------------------------fecth channel config peer1.fujitsu_bank.lu-----------------------------------
#docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@fujitsu_bank.lu/msp" peer1.fujitsu_bank.lu peer channel fetch config -o orderer.fujitsu_bank.lu:7050 -c bankschannel

#------------------------------Join peer1.fujitsu_bank.lu to the channel-----------------------------------
#docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@fujitsu_bank.lu/msp" peer1.fujitsu_bank.lu peer channel join -b bankschannel_config.block


#------------------------------install chaincode-----------------------------------
#docker exec cli peer chaincode install -n fujitsu_banks -v 1.0 -p github.com/hyperledger/fabric/BankChaincode

#------------------------------instantiate chaincode-----------------------------------
#docker exec cli peer chaincode instantiate -o orderer.fujitsu_bank.lu:7050  -C bankschannel -c '{"Args":[]}' -n fujitsu_banks -v 1.0 -P "OR('FujitsuBankLuMSP.member', 'FujitsuBankBeMSP.member')"

#sleep 30
#-------------------------------  Create the first client -------------------------------------------------------
#docker exec peer0.fujitsu_bank.lu peer chaincode invoke -C bankschannel -n fujitsu_banks -c '{"Args":["createClient","client-001","admin","Jean Mathieu","Tchamdjeu","jeanmathieu05.jm@gmail.com","Mathieu18"]}' --orderer orderer.fujitsu_bank.lu:7050

#docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@org1.fujitsu.com/msp" peer0.org1.fujitsu.com peer chaincode install -n bankChaincode -v 1.0 -p /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/github.com/Chaincode/  --orderer orderer.fujitsu.com:7050 -l golang



#-----------------------------update anchor peer number 0 of org1------------------------------------------------------
#docker exec peer0.org1.fujitsu.com peer channel update -o orderer.fujitsu.com:7050 -c fujitsuchannel -f /etc/hyperledger/configtx/Org1MSPanchors.tx

#./createPeerAdminCard.sh
#execute "afterStartFabric" script

#./afterStartFabric.sh

# CORE_CHAINCODE_LOGLEVEL=debug CORE_PEER_ADDRESS=peer0.org1.fujitsu.com:7051 CORE_CHAINCODE_ID_NAME=fujitsu_bank:0 $GOPATH/src/github.com/hyperledger/fabric/BankChaincode

# docker exec peer0.org1.fujitsu.com peer chaincode install -n fujitsu_bank -v 0 -p github.com/hyperledger/fabric/BankChaincode

#peer chaincode instantiate -n mycc1 -v 0 -p github.com/hyperledger/fabric/examples/chaincode/go/marbles02 -c '{"Args":["init"]}' -o 127.0.0.1:7050 -C ch1

# docker exec cli peer chaincode instantiate -o orderer.example.com:7050  -C fujitsuchannel -c '{"Args":[]}' -n fujitsu_bank -v 0 -P "OR('Org1MSP.member', 'Org2MSP.member')"


#*************************** upgrade chaicone ********************************************
#docker exec cli peer chaincode install -n fujitsu_bank -v 1.0 -p github.com/hyperledger/fabric/BankChaincode
#docker exec cli peer chaincode upgrade -n fujitsu_bank -v 2.0 -c '{"Args":[]}' -C fujitsuchannel

#****************************Check the certificate********************************************
#openssl x509 -in ./Desktop/HLF_test/crypto-config/peerOrganizations/fujitsu_bank.be/peers/peer0.fujitsu_bank.be/tls/ca.crt -text -noout

#*********************************create channel with tls*****************************************
#docker exec peer0.fujitsu_bank.lu peer channel create -c bankschannel --orderer orderer.fujitsu_bank.lu:7050 -f /etc/hyperledger/configtx/bankchannel.tx --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/fujitsu_bank.lu/orderers/orderer.fujitsu_bank.lu/msp/tlscacerts/tlsca.fujitsu_bank.lu-cert.pem

#docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@fujitsu_bank.lu/msp" peer0.fujitsu_bank.lu peer channel join -b bankschannel.block


#docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@fujitsu_bank.lu/msp" peer1.fujitsu_bank.lu peer channel fetch config -o orderer.fujitsu_bank.lu:7050 -c bankschannel --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/fujitsu_bank.lu/orderers/orderer.fujitsu_bank.lu/msp/tlscacerts/tlsca.fujitsu_bank.lu-cert.pem

#docker exec -e "CORE_PEER_MSPCONFIGPATH=/etc/hyperledger/msp/users/Admin@fujitsu_bank.lu/msp" peer1.fujitsu_bank.lu peer channel join -b bankschannel_config.block

#docker exec cli peer chaincode install -n fujitsu_banks -v 1.0 -p github.com/hyperledger/fabric/BankChaincode

#docker exec cli peer chaincode instantiate -o orderer.fujitsu_bank.lu:7050  -C bankschannel -c '{"Args":[]}' -n fujitsu_banks -v 1.0 -P "OR('FujitsuBankLuMSP.member', 'FujitsuBankBeMSP.member')" --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/fujitsu_bank.lu/orderers/orderer.fujitsu_bank.lu/msp/tlscacerts/tlsca.fujitsu_bank.lu-cert.pem

#sleep 15

#docker exec peer0.fujitsu_bank.lu peer chaincode invoke -C bankschannel -n fujitsu_banks -c '{"Args":["createClient","client-001","admin","Jean Mathieu","Tchamdjeu","jeanmathieu05.jm@gmail.com","Mathieu18"]}' --orderer orderer.fujitsu_bank.lu:7050 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/fujitsu_bank.lu/orderers/orderer.fujitsu_bank.lu/msp/tlscacerts/tlsca.fujitsu_bank.lu-cert.pem
